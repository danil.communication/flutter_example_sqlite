import 'package:employee_book/screen/add_employee_screen.dart'; // Импортируем файл с экраном добавления сотрудника
import 'package:employee_book/screen/home_screen.dart'; // Импортируем файл с главным экраном
import 'package:flutter/material.dart'; // Импортируем пакет flutter/material.dart для доступа к классам Flutter UI

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      // Начало оператора switch для определения маршрута
      case '/': // Случай, когда маршрут - главный экран
        return MaterialPageRoute(
            builder: (_) =>
                const HomeScreen()); // Возвращаем новый экземпляр класса MaterialPageRoute, который строит главный экран HomeScreen
      case '/add_employee': // Случай, когда маршрут - экран добавления сотрудника
        return MaterialPageRoute(
            builder: (_) =>
                const AddEmployeeScreen()); // Возвращаем новый экземпляр класса MaterialPageRoute, который строит экран добавления сотрудника AddEmployeeScreen
      default: // Случай по умолчанию, когда маршрут неизвестен
        return _errorRoute(); // Возвращаем результат приватного метода _errorRoute, который возвращает маршрут ошибки
    }
  }

  static Route<dynamic> _errorRoute() {
    return MaterialPageRoute(builder: (_) {
      // Возвращаем новый экземпляр класса MaterialPageRoute, который строит экран ошибки
      return Scaffold(
        // Создаем экземпляр Scaffold, который предоставляет основу для построения экрана
        appBar: AppBar(
          // Заголовок AppBar
          title: const Text('No Route'), // Заголовок со значением 'No Route'
          centerTitle: true, // Выравнивание заголовка по центру
        ),
        body: const Center(
          // Основное содержимое экрана, выравненное по центру
          child: Text(
            'Sorry no route was found!', // Текст сообщения об ошибке
            style: TextStyle(
                color: Colors.red,
                fontSize:
                    18.0), // Стиль текста с красным цветом и размером шрифта 18
          ),
        ),
      );
    });
  }
}
