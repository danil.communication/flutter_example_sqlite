import 'package:employee_book/routes/route_generator.dart';
import 'package:employee_book/screen/home_screen.dart';
import 'package:flutter/material.dart';

// Импортируем пакеты "firstapp/routes/route_generator.dart", "firstapp/screen/home_screen.dart" и "flutter/material.dart", которые содержат необходимые классы и функции для разработки приложения с помощью Flutter.

void main() {
  runApp(const MyApp());
  // Главная функция "main", которая запускает приложение.
  // Вызываем функцию "runApp", которая принимает виджет "MyApp" в качестве аргумента и запускает приложение.
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  // Объявляем класс "MyApp", который является виджетом и наследуется от класса "StatelessWidget".
  // Ключевое слово "const" указывает, что конструктор класса является константным.
  // Конструктор принимает необязательный аргумент "key" типа "Key", который используется для уникальной идентификации виджета.
  // Конструктор вызывает конструктор базового класса "super" и передает ему аргумент "key".

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      // Устанавливаем заголовок приложения.
      theme: ThemeData(
        primarySwatch: Colors.blue,
        // Устанавливаем тему приложения, в данном случае голубой цвет.
      ),
      initialRoute: '/',
      // Устанавливаем начальный путь приложения.
      onGenerateRoute: RouteGenerator.generateRoute,
      // Устанавливаем функцию-генератор маршрутов, которая будет вызываться при неизвестном маршруте.
    );
  }
}
